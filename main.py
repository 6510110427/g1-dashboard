from dash import Dash, dcc, html,Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas
import plotly.graph_objects as go


app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

colors = {"background": "#111111", "text": "#7FDBFF"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
th_json = "https://raw.githubusercontent.com/apisit/thailand.json/master/thailand.json"
df = pandas.read_excel("data/highway.xlsx")
df = df.dropna()
df.columns = ['HighWayNumber',
              'HighWayName', 
              'SmallCars',
              'MediumCars', 
              'LargeCars', 
              'SmallTruck', 
              'MediumTruck', 
              'BigTruck', 
              'Trailer', 
              'SemiTrailer', 
              'Motorcycle', 
              'Total', 
              'Province', 
              'Region']
df = pandas.melt(df, id_vars=['Province','Region','Total'], 
                        value_vars=['SmallCars',	
                                    'MediumCars',	
                                    'LargeCars',	
                                    'SmallTruck',	
                                    'MediumTruck',	
                                    'BigTruck',	'Trailer',	
                                    'SemiTrailer',	
                                    'Motorcycle'], 
                        var_name='type', 
                        value_name='value')


# fig = px.choropleth_mapbox(df, geojson=th_json, color="value",
#                            locations="provinces", featureidkey="properties.name",
#                            center={"lat": 13.736717, "lon": 100.523186},
#                            mapbox_style="open-street-map", zoom=4)
# fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
# fig.show()
#สร้างเเมปเล่นๆ

@app.callback(
    Output("pie-graph", "figure"),
    Input("region", "value"),
    Input("province", "value"),
    Input("selectgraph", "value")
)
def show_data(region,province,selectgraph):
    choosere = df.loc[(df['Region'] == region) & (df['Province'] == province)]
    if selectgraph == 'Donut':
        
        fig = px.pie(
        choosere,
        values='value',
        names='type', # use the 'Province' column of the filtered dataframe
        color_discrete_sequence=["#64C2A6", "#7031AC", "#C94D6D", "#E4BF58", "#4174C9"],
        hole= 0.4,
        
        )
        fig.update_traces(
            direction='clockwise',
            rotation=90,
            pull=0.05,
        )
        fig.update_layout(paper_bgcolor = "#f5f3ef",
        title_text='Highway Utilization Rate',annotations=[dict(text=province, x=0.5, y=0.5, font_size=14, showarrow=False)]),
        return fig
    
    elif selectgraph == 'Scatter':
        
        fig = px.scatter(
            choosere,
            x='type',
            y='Total',
            color='Province'

        )
        fig.update_layout(paper_bgcolor = "#f5f3ef")
        return fig
    
# def show_data(region,province):
#         choosere = df.loc[(df['Region'] == region) & (df['Province'] == province)]
        
#         fig = px.pie(
#         choosere,
#         values='value',
#         names='type', # use the 'Province' column of the filtered dataframe
#         color_discrete_sequence=["#003F5C", "#58508D", "#BC5090", "#FF6361", "#FFA600"],
#         hole= 0.4
#         )
#         fig.update_layout(paper_bgcolor = "#e2d0bc")
#         return fig
    

    



app.layout = html.Div(
     style={
    "background":"#e2d0bc",
    "position": "flex",
    "width" : "100vw",
    "height" : "100vh",
    "margin" : "0",
    "border" : "0",
    },
    children=[
        html.Div(
            children=[
                html.H1(
                    children="Group-1",
                    style={
                        # "textAlign": "center",
                        "text-Align": "center",
                            "text": "#ffffe0",
                         "background":"#47423c",
                                "padding":" 15px 18px",
                                "text-decoration": "none",
                                "color": "#ffffe0",
                                "display": "flex",
                                "margin":" 15px 0",
                                "margin-left":" 20px",
                                # "margin-top": "30%",
                                "border-radius": "5px",
                                # "column-gap": "normal",
                                # "position": "absolute",
                                # "top": "14%",
                                "width" : "18vw",
                                "transition":" 0.3s",
                                "hover" : "white",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Highway Data",
                    style={
                        "text-Align": "center",
                         "background":"#262321",
                                "padding":" 10px 18px",
                                "text-decoration": "none",
                                "color": "#ffffe0",
                                "display": "flex",
                                "margin":" 10px 0",
                                "margin-left":" 20px",
                                "margin-top": "5px",
                                "border-radius": "5px",
                                # "column-gap": "normal",
                                # "position": "absolute",
                                # "top": "14%",
                                "transition":" 0.3s",
                                "width" : "97vw",
                                "hover" : "color = white"
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id="pie-graph"),
                    ],
                    className="col-8",
                    style={
                    "background":"#f5f3ef",
                    "border-radius": "5px",
                    "hover" : "black",
                    "margin-left":" 20px",
                    "width" : "65vw",
                        # 'plot_bgcolor': 'rgba(0,0,0,0)',
                        # 'paper_bgcolor': 'rgba(0,0,0,0)',
                    }
                ),
                html.Div(
                    [
                         dbc.RadioItems(
                                options=[
                                    dict(label=p.strip(), value=p.strip())
                                    for p in df["Region"].unique()
                                ],
                                value=1,
                                id="region",
                                inline=True,
                                style={
                                "text-Align": "center",
                                "margin-top": "5px",
                                "color": "#262421",
                                
                            },
                                
                        ),
                        # dbc.Checklist(
                        #     options=[
                        #         dict(label=z.strip(), value=z.strip())
                        #         for z in df["Region"].unique()
                        #     ],
                        #     value=df["Region"][0],
                        #     id="region",
                        #     inline=True,
                        # ),
                        # dbc.Select(
                        #     options=[
                        #         dict(label=z.strip(), value=z.strip())
                        #         for z in df["Region"].unique()
                        #     ],
                        #     id="region",
                        #     value=df["Region"][0],
                        # ),
                    
                        dbc.Select(
                            options=[
                                dict(label=p.strip(), value=p.strip())
                                for p in df["Province"].unique()
                            ],
                            id="province",
                            value=df["Province"][0],
                            style= {
                                "text-Align": "center",
                                "margin-top": "8px",
                                "background":"#252525",
                                "color": "#ffffe0",
                                "width" : "11.5vw",
                            },
                            # searchable=True
            
                        ),
                        # dbc.RadioItems(
                        #         options=[
                        #             dict(label=p.strip(), value=p.strip())
                        #             for p in df["Province"].unique()
                        #         ],
                        #         value=1,
                        #         id="province",
                        #         inline=True,
                        # ),
                        dbc.Select(
                                options=[
                                    dict(label=p.strip(), value=p.strip())
                                    for p in ['Donut','Scatter']]
                                ,
                                value=['Donut','Scatter'][0],
                                id="selectgraph",
                                # inline=True,
                                style={
                                "text-Align": "center",
                                "margin-top": "8px",
                                "width" : "8vw",
                                "position": "inline-center",
                                "background":"#252525",
                                "color": "#ffffe0",

                                
                            },
                                
                        ),
                    ],
                    className="col-4",
                ),
            ],
            className="row",
        ),
        
    ],
    className="container-fluid",
)

if __name__ == "__main__":
    app.run_server(debug=True)
