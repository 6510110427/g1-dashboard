from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas


# app = Dash(__name__)
app = Dash(__name__, external_stylesheets=[dbc.themes.QUARTZ])

# colors = {"background": "#111111", "text": "#7FDBFF"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("data/refdata.xlsx")
df.columns = [
    "BE",
    "emission_type",
    "category",
    "sub_category",
    "en_sub_category",
    "quantity",
]
df["quantity"] = df["quantity"].replace("-", 0)
# power = df[df["emission_type"].str.contains("ภาคพลังงาน")]


@app.callback(
    Output("pie-graph", "figure"),
    Input("year", "value"),
    Input("emission_type", "value"),
)
def show_data(selected_year, emission_type):
    print(selected_year, emission_type)
    filtered_df = df[df["BE"] == int(selected_year)][
        df["emission_type"].str.contains(emission_type)
    ]
    # print(filtered_df)
    fig = px.pie(
        filtered_df,
        values="quantity",
        names="category",
        color_discrete_sequence=["#5cb85c", "#5bc0de", "#ffc107"],
        color="sub_category",
    )
    fig.update_layout(paper_bgcolor = "#e6e6fa")
    # fig.update_layout(transition_duration=500)

    return fig

# app.

app.layout = html.Div(
    style={
    "background":"#e6e6fa",
    "position": "flex",
    "width" : "100vw",
    "height" : "100vh",
    "margin" : "0",
    "border" : "0",
    },
    children=[
        html.Div(
            children=[
                html.H1(
                    children="G1",
                    style={
                        # "textAlign": "center",
                         "background":"#252525",
                                "padding":" 10px 18px",
                                "text-decoration": "none",
                                "color": "rgb(227, 220, 255)",
                                "color": "rgb(227, 220, 255)",
                                "display": "flex",
                                "margin":" 15px 0",
                                "margin-left":" 20px",
                                # "margin-top": "30%",
                                "border-radius": "5px",
                                # "column-gap": "normal",
                                # "position": "absolute",
                                # "top": "14%",
                                "transition":" 0.3s",
                                "hover" : "white",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="highway",
                    style={
                        "textAlign": "center",
                         "background":"#252525",
                                "padding":" 10px 18px",
                                "text-decoration": "none",
                                "color": "rgb(227, 220, 255)",
                                "color": "rgb(227, 220, 255)",
                                "display": "flex",
                                "margin":" 10px 0",
                                "margin-left":" 20px",
                                "margin-top": "5px",
                                "border-radius": "5px",
                                # "column-gap": "normal",
                                # "position": "absolute",
                                # "top": "14%",
                                "transition":" 0.3s",
                                "hover" : "color = white"
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id="pie-graph"),
                    ],
                    className="col-8",
                    style={
                    "background":"#e6e6fa",
                        # 'plot_bgcolor': 'rgba(0,0,0,0)',
                        # 'paper_bgcolor': 'rgba(0,0,0,0)',
                        
                    }
                ),
                html.Div(
                    [
                        # dbc.Dropdown(df["BE"].unique(), df["BE"].min(), id="year"),
                        # dbc.Dropdown(
                        #     df["emission_type"].unique(),
                        #     df["emission_type"][0],
                        #     id="emission_type",
                        # ),
                        dbc.Select(
                            options=[
                                dict(label=be, value=be) for be in df["BE"].unique()
                            ],
                            id="year",
                            value=df["BE"].min(),
                        ),
                        dbc.Select(
                            options=[
                                dict(label=et.strip(), value=et.strip())
                                for et in df["emission_type"].unique()
                            ],
                            id="emission_type",
                            value=df["emission_type"][0],
                            style= {
                                "margin-top": "5px",
                                "background":"#252525",
                                "color": "rgb(227, 220, 255)",
                            },
                        ),
                    ],
                    className="col-4",
                ),
            ],
            className="row",
        ),
    ],
    className="container-fluid",
)

if __name__ == "__main__":
    app.run_server(debug=True)

